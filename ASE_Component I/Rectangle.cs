﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Component_I
{
    /// <summary>
    /// class to draw Rectangle
    /// </summary>
    class Rectangle : Shape
    {
        private int x, y, w, h;
        /// <summary>
        /// method to save the values
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void saved_values(int a, int b, int c, int d)
        {
            x = a;
            y = b;
            w = c;
            h = d;
        }
       /// <summary>
       /// methos to draw the shape
       /// </summary>
       /// <param name="g"></param>
        public override void Draw_shape(Graphics g)
        {
            Pen pen = Form1.defaultpen;    //pen set to deafault. i.e. black   
            SolidBrush brush = Form1.sb;   //brush set to deafault. i.e. black   
            if (Form1.fill)
            {
                g.FillRectangle(brush, x, y, w, h);    //filled rectangle drawn
            }
            else
            {
                g.DrawRectangle(pen, x, y, w, h);      //not filled rectangle drawn
            }

        }
    }

}