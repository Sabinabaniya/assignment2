﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;

namespace ASE_Component_I
{
    
    public partial class Form1 : Form
    {

        /// <summary>
        /// Position of Xaxis
        /// </summary>
        public int positionXaxis = 0;
        /// <summary>
        /// Position of Yaxis
        /// </summary>
        public int positionYaxis = 0;
        string[] shapes = { "drawto", "moveto", "rectangle", "circle", "triangle", "pen", "fill", "square" };
        public bool draw = false;
        public bool load = false;
        bool method = false;
        public bool save = false;
        public bool execute = false;
        public bool clear_bool = false;

        List<String[]> n = new List<String[]>();
        List<String> body = new List<string>();
        List<String> b = new List<String>();
        List<int> p = new List<int>();
        List<String> pi = new List<String>();


        public bool reset_bool = false;
        public int lineCount = 1;
        public int lineNumberCount = 0;
        public int IfCounter = 0;
        public Dictionary<string, string> variableDict = new Dictionary<string, string>();
        String[] m_syntax;
        public static bool fill = false;
        
        public static Pen defaultpen = new Pen(Color.Black);
        
        public static SolidBrush sb = new SolidBrush(Color.Black);
        /// <summary>
        /// method to change the pain.
        /// </summary>
        /// <param name="color"></param>
        public void penswitcher(string color)
        {
            if (!fill)
            {
                switch (color)                     //color assigned in switch
                {
                    case "blue":
                        {
                            defaultpen = new Pen(Color.Blue);
                            break;
                        }

                    case "green":
                        {
                            defaultpen = new Pen(Color.Green);
                            break;
                        }

                    case "yellow":
                        {
                            defaultpen = new Pen(Color.Yellow);
                            break;
                        }
                    case "red":
                        {
                            defaultpen = new Pen(Color.Red);
                            break;
                        }

                }
                MessageBox.Show("Pen color changed to " + color);   //after changing pen color.

            }

            else
            {

                switch (color)
                {
                    case "blue":
                        {
                            sb = new SolidBrush(Color.Blue);
                            break;
                        }

                    case "green":
                        {
                            sb = new SolidBrush(Color.Green);
                            break;
                        }

                    case "yellow":
                        {
                            sb = new SolidBrush(Color.Yellow);
                            break;
                        }

                    case "red":
                        {
                            sb = new SolidBrush(Color.Red);
                            break;
                        }

                }


            }
        }
        /// <summary>
        /// method to draw the triangle
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="v3"></param>
        public void triangle_draw(int v1, int v2, int v3)
        {
            throw new NotImplementedException();
        }

        public Form1()
        {
            InitializeComponent();
        }
       
        /// <summary>
        /// method to move the pen
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void pentomove(int x, int y)
        {
            Pen pen_move = new Pen(Color.Black, 2);
            positionXaxis = x;
            positionYaxis = y;




        }
        /// <summary>
        /// method for getting commands
        /// </summary>
        public void suj()
        {
            String command = textBox2.Text;
            String[] multiLine = command.Split(')');

            for (int i = 0; i < multiLine.Length - 1; i++)
            {
                String abc = multiLine[i].Trim();
                m_syntax = abc.Split('(');

                if (m_syntax[0].Equals("pen") == true)
                {
                    this.penswitcher(m_syntax[1]);
                }

                else if (m_syntax[0].Equals("fill") == true)
                {
                    if (m_syntax[1] == "on")
                    {
                        fill = true;

                    }

                    if (m_syntax[1] == "off")
                    {
                        fill = false;

                    }

                }

            }
            if (string.Compare(m_syntax[0].ToLower(), "moveto") == 0)
            {
                String[] parameter1 = m_syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("MoveTo Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String[] parameter2 = parameter1[1].Split(')');
                    String one = parameter1[0];
                    String two = parameter2[0];
                    pentomove(int.Parse(one), int.Parse(two));
                    if (parameter1.Length > 1 && parameter1.Length < 3)
                        pentomove(int.Parse(one), int.Parse(two));
                    else
                        throw new ArgumentException("MoveTo takes Only 2 Parameters");

                }
            }
            else if (m_syntax[0].Equals("\n"))
            {

            }
            //executes if "drawto" command is triggered
            else if (string.Compare(m_syntax[0].ToLower(), "drawto") == 0)
            {

                String[] parameter1 = m_syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("DrawTo Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String[] parameter2 = parameter1[1].Split(')');
                    String one = parameter1[0];
                    String two = parameter2[0];
                    pentodraw(int.Parse(one), int.Parse(two));
                    if (parameter1.Length == 2)
                        pentodraw(int.Parse(one), int.Parse(two));

                    else
                    {
                        throw new ArgumentException("DrawTo Takes Only 2 Parameters");
                    }
                }

            }
            //executes if "clear()" command is triggered
            else if (string.Compare(m_syntax[0].ToLower(), "clear") == 0)
            {
                clear();
            }
            //executes if "reset()" command is triggered
            else if (string.Compare(m_syntax[0].ToLower(), "reset") == 0)
            {
                reset();
            }
            //executes if "rectangle" command is triggered
            else if (string.Compare(m_syntax[0].ToLower(), "rectangle") == 0)
            {

                String[] parameter1 = m_syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("Rectangle Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String[] parameter2 = parameter1[1].Split(')');
                    String one = parameter1[0];
                    String two = parameter2[0];
                    if (parameter1.Length > 1 && parameter1.Length < 3)
                        rectangle_draw(positionXaxis, positionYaxis, int.Parse(one), int.Parse(two));
                    else
                        throw new ArgumentException("Rectangle Takes Only 2 Parameters");
                }
            }
            else if (string.Compare(m_syntax[0].ToLower(), "circle") == 0)
            {
                String test = m_syntax[1];
                String[] parameter2 = m_syntax[1].Split(')');
                if (!test.Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    String two = parameter2[0];
                    if (two != null || two != "" || two != " ")
                        circle_draw(positionXaxis, positionYaxis, int.Parse(two));
                    else
                        throw new ArgumentException("Circle Takes Only 1 Parameter");

                }
            }
            //executes if "square" command is triggered
            else if (string.Compare(m_syntax[0].ToLower(), "square") == 0)
            {
                String test = m_syntax[1];
                String[] parameter2 = m_syntax[1].Split(')');
                if (!test.Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    shapefactory sh = new shapefactory();
                    Shape c1 = sh.GetShape("square");
                    Graphics g = panel1.CreateGraphics();
                    c1.Draw_shape(g);
                    String two = parameter2[0];
                    if (two != null || two != "" || two != " ")
                        square_draw(positionXaxis, positionYaxis, int.Parse(two));
                    else
                        throw new ArgumentException("Square Takes Only 1 Parameter");

                }
            }
            //executes if "triangle" command is triggered
            else if (string.Compare(m_syntax[0].ToLower(), "triangle") == 0)
            {
                String[] parameter1 = m_syntax[1].Split(',');
                //String[] parameter2 = m_syntax[1].Split(',');
                if (parameter1.Length != 2)
                    throw new Exception("Triangle Takes Only 2 Parameters");
                else if (!parameter1[parameter1.Length - 1].Contains(')'))
                    throw new Exception(" " + "Missing Paranthesis!!");
                else
                {
                    shapefactory sh = new shapefactory();
                    Shape c1 = sh.GetShape("triangle");
                    String[] parameter3 = parameter1[1].Split(')');
                    String one = parameter1[0];
                   // String two = parameter2[0];
                    String three = parameter3[0];
                    if (parameter1.Length > 1 && parameter1.Length < 3)
                        triangle_draw(positionXaxis, positionYaxis, int.Parse(one), int.Parse(three));
                    else
                        throw new ArgumentException("Triangle Takes Only 2 Parameters");
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// method for changing position
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
       
        public void pentodraw(int a, int b)
        {
            Pen mew = new Pen(Color.Black, 2);
            Graphics g = panel1.CreateGraphics();
            g.DrawLine(mew, positionXaxis, positionYaxis, a, b);
            positionXaxis = a;
            positionYaxis = b;
        }
        /// <summary>
        /// method for checking commands
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkCommand(string line)
        {
            for (int a = 0; a < shapes.Length; a++)
            {
                if (line.Contains(shapes[a]))
                {


                    return true;
                }
            }
            String[] temp = line.Split('(');
            if (b.Contains(temp[0]))
            {
                return true;
            }
            return false;
        }

       /// <summary>
       /// it helps to run the button
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        public void button2_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == "clear")
            {
                clear();
            }

            if (textBox1.Text == "reset")
            {
                reset();
            }


            if (textBox1.Text == "run")
            {
                variableDict.Clear();
                method = false;

                lineCount = 1;
                panel1.Refresh();
                textBox1.Clear();
                execute = false;
                var multi_command = textBox2.Text;
                string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                String command = textBox2.Text;
                String[] multiLine = command.Split(')');

                for (int i = 0; i < multiLine.Length - 1; i++)
                {
                    String abc = multiLine[i].Trim();
                    m_syntax = abc.Split('(');

                    if (m_syntax[0].Equals("pen") == true)
                    {
                        this.penswitcher(m_syntax[1]);
                    }

                    else if (m_syntax[0].Equals("fill") == true)
                    {
                        if (m_syntax[1] == "on")
                        {
                            fill = true;

                        }

                        if (m_syntax[1] == "off")
                        {
                            fill = false;

                        }

                    }

                }



                foreach (string l in multi_syntax)
                {


                    bool result = caseRun(l);
                    if (!result)
                    {
                        panel1.Invalidate();
                        break;
                    }


                    lineCount++;

                }


            }

        }

        public bool caseRun(string line)
        {
            line = line.ToLower().Trim();



            if (IfCounter != 0)
            {
                IfCounter--;
                return true;

            }

            if (lineNumberCount != 0)
            {

                lineNumberCount--;

                return true;
            }

            else if (checkCommand(line))
            {

                string[] m_syntax = line.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
                if (!runShape(m_syntax, line))
                    return false;


            }
            else if (checkVariableDec(line))
            {
                try
                {
                    string[] variableDeclration = line.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);
                    string key = variableDeclration[0];
                    string value = variableDeclration[1];


                    variableDict.Add(key, value);



                }
                catch (Exception e)
                {
                    textBox1.Text = "Line: " + lineCount + " Invalid Declaration of variable";
                    return false;
                }


            }
            else if (checkIfElse(line))
            {
                bool endIfCheck = false;
                bool conditionStatus = false;
                string conditionOperator = "";
                try
                {

                    string[] IfCondtionParameter = getIfParameter(line);

                    foreach (string c in IfCondtionParameter)
                    {
                        Console.WriteLine(c);
                    }


                    if (!variableDict.ContainsKey(IfCondtionParameter[0].Trim().ToLower()))
                    {
                        textBox1.Text = "Line: " + lineCount + " Variable doesn't exist";
                        return false;
                    }


                    string condValueString = IfCondtionParameter[1];
                    int condValue = Int32.Parse(condValueString);




                    string v = IfCondtionParameter[0].Trim().ToLower();
                    string varValuestring = variableDict[v];
                    int varvalue = Int32.Parse(varValuestring);

                    if (line.Contains("="))
                    {
                        conditionOperator = "=";

                    }
                    else if (line.Contains("<"))
                    {
                        conditionOperator = "<";
                    }
                    else if (line.Contains(">"))
                    {
                        conditionOperator = ">";
                    }





                    var multi_command = textBox2.Text;
                    string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string l in multi_syntax)
                    {

                        if (l.ToLower().Trim() == "endif")
                        {
                            endIfCheck = true;
                            break;
                        }

                    }
                    if (!endIfCheck)
                    {
                        textBox1.Text = "Line: " + lineCount + " If EndIf statement not closed.";
                        return false;
                    }


                    if (conditionOperator == "=")
                    {
                        if (varvalue == condValue)
                        {
                            conditionStatus = true;
                        }
                    }
                    else if (conditionOperator == "<")
                    {


                        if (varvalue < condValue)
                        {
                            conditionStatus = true;
                        }
                    }
                    else if (conditionOperator == ">")
                    {
                        if (varvalue > condValue)
                        {
                            conditionStatus = true;
                        }
                    }
                    for (int i = lineCount; i < multi_syntax.Length; i++)
                    {
                        if (multi_syntax[i] == "endif")
                        {
                            break;
                        }
                        else
                        {
                            IfCounter++;
                        }
                    }

                    if (conditionStatus)
                    {
                        IfCounter = 0;
                    }

                }
                catch (Exception e)
                {
                    textBox1.Text = "Line: " + lineCount + " Invalid if else statement" + "\n" + e.Message;
                    return false;
                }






            }
            else if (line == "endif")
            {
                return true;
            }
            else if (checkVariableOperation(line))
            {



                //check variable operation
                string[] variable = line.Split(new char[] { '+', '-' }, 2, StringSplitOptions.RemoveEmptyEntries);
                string variableOperator = "";

                if (line.Contains("+"))
                {
                    variableOperator = "+";

                }
                else
                {
                    variableOperator = "-";
                }
                string realKey = variable[0];
                int realValue = Int32.Parse(variable[1]);
                int dictValue = Int32.Parse(variableDict[realKey]);
                if (!variableDict.ContainsKey(realKey))
                {
                    textBox1.Text = "Line: " + lineCount + " Variable doesn't Exist";
                    return false;
                }


                if (variableOperator == "+")
                {
                    variableDict[realKey] = (dictValue + realValue).ToString();
                }
                else
                {
                    variableDict[realKey] = (dictValue - realValue).ToString();
                }

            }
            else if (checkLoop(line))
            {

                bool endloopCheck = false;  //boolen to check the end of loop
                try
                {
                    string[] loop = line.Split(new string[] { "for" }, 2, StringSplitOptions.RemoveEmptyEntries);

                    string loopCondition = loop[1].Trim();

                    string[] loopVariable = loopCondition.Split(new string[] { "<=", ">=" }, 2, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string l in loopVariable)
                    {
                        Console.WriteLine(l);
                    }

                    foreach (KeyValuePair<string, string> k in variableDict)
                    {
                        Console.WriteLine(k.Key + "=" + k.Value);
                    }
                    int loopValue = Int32.Parse(loopVariable[1].Trim());
                    string loopKey = loopVariable[0].Trim();
                    Console.WriteLine(loopKey);
                    if (!variableDict.ContainsKey(loopKey))
                    {
                        textBox1.Text = "Line: " + lineCount + " Variable doesn't exist 2";
                        return false;
                    }

                    
                    var multi_command = textBox2.Text;
                    string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string l in multi_syntax)
                    {

                        if (l.ToLower().Trim() == "endloop")
                        {
                            endloopCheck = true;
                            break;
                        }

                    }
                    if (!endloopCheck)
                    {
                        textBox1.Text = "Line: " + lineCount + " loop not closed.";
                        return false;
                    }


                    //endloop check



                    int counterLine = 0;
                    int lineNumberCount1 = 0;

                    List<string> loopList = new List<string>();
                    for (int i = lineCount; i < multi_syntax.Length; i++)
                    {

                        if (multi_syntax[i] == "endloop")
                        {

                            break;
                        }
                        else
                        {
                            lineNumberCount1++;
                            loopList.Add(multi_syntax[i]);
                        }
                    }



                    int dictValue = 0;
                    string loopOperator = "";
                    counterLine = lineCount;

                    if (line.Contains("<="))
                    {
                        loopOperator = "<=";
                    }
                    else
                    {
                        loopOperator = ">=";
                    }


                    if (loopOperator == "<=")
                    {
                        while (dictValue <= loopValue)
                        {
                            lineCount = counterLine;
                            foreach (string list in loopList)
                            {
                                lineCount++;
                                if (!caseRun(list))
                                    return false;
                            }
                            dictValue = Int32.Parse(variableDict[loopKey]);
                        }
                    }
                    else
                    {

                        while (dictValue >= loopValue)
                        {
                            lineCount = counterLine;
                            foreach (string list in loopList)
                            {
                                lineCount++;
                                if (!caseRun(list))
                                    return false;
                            }
                            dictValue = Int32.Parse(variableDict[loopKey]);
                        }
                    }

                    lineCount = counterLine;
                    lineNumberCount = lineNumberCount1;
                }
                catch (Exception e)
                {
                    textBox1.Text = "Line: " + lineCount + " Invaild Loop Statement + \n " + e.Message;
                    return false;
                }

            }
            else if (line == "endloop")
            {
                return true;
            }
            else if (checkMethod(line))
            {
                bool endMethodCheck = false;
                var multi_command = textBox2.Text;
                string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = lineCount; i < multi_syntax.Length; i++)
                {

                    if (multi_syntax[i] == "endmethod")
                    {
                        endMethodCheck = true;
                        break;
                    }

                }

                if (!endMethodCheck)
                {
                    textBox1.Text = "Line: " + lineCount + "Method not closed";
                    return false;
                }



                String[] area = multi_command.Split('\n');
                String snj1 = "";
                String snj2 = "";


                for (int i = 0; i < area.Length; i++)
                {
                    if (area[i].ToLower().Contains("method"))
                    {

                        String[] t = area[i].Split(' ');
                        String[] nn = t[t.Length - 1].Split('(');
                        if (nn[nn.Length - 1].Contains(','))
                        {
                            String[] g = nn[nn.Length - 1].Split(',');
                            p.Add(g.Length);
                            for (int x = 0; x < g.Length; x++)
                            {
                                if (x == g.Length - 1)
                                {
                                    String[] tt = g[x].Split(')');
                                    snj2 = snj2 + tt[0] + "\n";
                                }
                                else
                                {
                                    snj2 = snj2 + g[x] + "\n";
                                }
                            }
                            pi.Add(snj2);
                        }
                        else
                        {
                            String[] g = nn[nn.Length - 1].Split(')');
                            if (g[0] == " " || g[0] == null || g[0] == "")
                            {
                                p.Add(0);
                                pi.Add(" ");
                            }
                            else
                            {
                                p.Add(1);
                                snj2 = snj2 + g[0] + "\n";
                                pi.Add(snj2);
                            }
                        }
                        b.Add(nn[0].ToLower());

                    }
                    else
                    {
                        snj1 = snj1 + area[i] + "\n";
                    }
                }

                body.Add(snj1);
                n.Add(snj1.Split('\n'));
                method = true;


            }
            else if (line == "endmethod")
            {

            }
            else
            {
                textBox1.Text = "Line: " + lineCount + " Command doesn't Exist";
                return false;
            }


            return true;
        }

        /// <summary>
        /// method to chek the loop
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkLoop(string line)
        {
            if (line.StartsWith("loop"))
                return true;

            return false;

        }

        public string[] getIfParameter(string line)   //method for getting parameter
        {

            int start = line.IndexOf("(") + 1;
            int end = line.IndexOf(")", start);

            string result = line.Substring(start, end - start);
            string[] parameterList = result.Split(new Char[] { '>', '<', '=' }, 2, StringSplitOptions.RemoveEmptyEntries);

            return parameterList;



        }

        /// <summary>
        /// Method to check the variable operation
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkVariableOperation(string line)
        {
            if (line.Contains("+") || line.Contains("-"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// method for getting parameter
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public string[] getParameter(string line)
        {

            int start = line.IndexOf("(") + 1;
            int end = line.IndexOf(")", start);

            string result = line.Substring(start, end - start);
            string[] parameterList = result.Split(new Char[] { ',' }, 2, StringSplitOptions.RemoveEmptyEntries);

            return parameterList;


        }
        /// <summary>
        /// method to check if else statement.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkIfElse(string line)
        {
            if (line.StartsWith("if"))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// method to check the methods
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkMethod(string line)
        {
            if (line.StartsWith("method"))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// method to check the variables
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool checkVariableDec(string line)
        {
            if (line.Contains("=") && !line.StartsWith("if") && !line.StartsWith("method") && !line.StartsWith("loop"))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// method to get variables
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>

        public bool withVariable(string[] lines)
        {
            string line = "(" + lines[1];
            string[] parameters = getParameter(line);

            foreach (string param in parameters)
            {
                Console.WriteLine(param);
            }
            bool variableCheck = false;

            foreach (string param in parameters)
            {
                bool isNumeric = int.TryParse(param, out _);

                if (!isNumeric)
                {
                    variableCheck = true;
                    if (!variableDict.ContainsKey(param))
                    {
                        return false;
                    }
                    else
                    {
                        line = line.Replace(param, variableDict[param]);
                    }
                }
            }



            if (!variableCheck)
                return false;

            line = lines[0] + line;
            Console.WriteLine(line);
            caseRun(line);


            return true;
        }
        /// <summary>
        /// method for running shape
        /// </summary>
        /// <param name="m_syntax"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool runShape(string[] m_syntax, String line)
        {

            if (!method)
            {
                if (withVariable(m_syntax))
                {
                    return true;

                }
            }


            try
            {

                if (!method)
                {
                    if (b.Contains(m_syntax[0].ToLower()))
                    {
                        String mina = line;

                        String mino = mina.Split('(')[0].Trim();
                        if (b.Contains(mino.Trim()))
                        {
                            int count = 0;
                            String[] l = mina.Split('(');
                            List<int> van = new List<int>();
                            if (l[l.Length - 1].Contains(','))
                            {
                                String[] alex = l[l.Length - 1].Split(',');
                                count = alex.Length;
                                for (int q = 0; q < alex.Length; q++)
                                {
                                    if (q != alex.Length - 1)
                                    {
                                        van.Add(int.Parse(alex[q]));
                                    }
                                    else
                                    {
                                        String[] fabin = alex[q].Split(')');

                                        if (fabin[0] != "" && fabin[0] != " " && fabin[0] == null)
                                            van.Add(int.Parse(fabin[0]));
                                        else
                                            van.Add(int.Parse(fabin[0]));
                                    }
                                }

                                for (int k = 0; k < van.Count(); k++)
                                {

                                }
                            }
                            else
                            {
                                String[] g = l[l.Length - 1].Split(')');
                                if (g[0] == " " || g[0] == null || g[0] == "")
                                {
                                    count = 0;
                                }
                                else
                                {
                                    van.Add(int.Parse(g[0]));
                                    count = 1;
                                }
                            }
                            if (count == p[b.IndexOf(mino.Trim())])
                            {
                                String[] rob = body[b.IndexOf(mino.Trim())].Split('\n');
                                String son = pi[b.IndexOf(mino.Trim())];

                                for (int salah = 0; salah < rob.Length; salah++)
                                {
                                    if (!string.IsNullOrEmpty(rob[salah]))
                                    {
                                        if (son == " ")
                                        {

                                            caseRun(rob[salah]);
                                        }
                                        else
                                        {
                                            String[] io = son.Split('\n');
                                            for (int ui = 0; ui < io.Length; ui++)
                                            {
                                                if (!string.IsNullOrEmpty(io[ui]))
                                                {
                                                    if (rob[salah].Contains(io[ui].Trim()))
                                                    {

                                                        rob[salah] = rob[salah].Replace(io[ui].Trim(), van[ui].ToString());

                                                    }
                                                }
                                                else
                                                {
                                                    caseRun(rob[salah]);
                                                }
                                            }

                                            caseRun(rob[salah]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Parameter Doesn't Match");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Not Found");
                        }
                    }
                    //executes if "moveto" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "moveto") == 0)
                    {
                        String[] parameter1 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("MoveTo Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String[] parameter2 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            String two = parameter2[0];
                            pentomove(int.Parse(one), int.Parse(two));
                            if (parameter1.Length > 1 && parameter1.Length < 3)
                                pentomove(int.Parse(one), int.Parse(two));
                            else
                                throw new ArgumentException("MoveTo takes Only 2 Parameters");

                        }
                    }
                    else if (m_syntax[0].Equals("\n"))
                    {

                    }
                    //executes if "drawto" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "drawto") == 0)
                    {

                        String[] parameter1 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("DrawTo Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String[] parameter2 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            String two = parameter2[0];
                            pentodraw(int.Parse(one), int.Parse(two));
                            if (parameter1.Length == 2)
                                pentodraw(int.Parse(one), int.Parse(two));

                            else
                            {
                                throw new ArgumentException("DrawTo Takes Only 2 Parameters");
                            }
                        }

                    }
                    //executes if "clear()" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "clear") == 0)
                    {
                        clear();
                    }
                    //executes if "reset()" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "reset") == 0)
                    {
                        reset();
                    }
                    //executes if "rectangle" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "rectangle") == 0)
                    {
                        String[] parameter1 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("Rectangle Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String[] parameter2 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            String two = parameter2[0];
                            if (parameter1.Length > 1 && parameter1.Length < 3)
                                rectangle_draw(positionXaxis, positionYaxis, int.Parse(one), int.Parse(two));
                            else
                                throw new ArgumentException("Rectangle Takes Only 2 Parameters");
                        }
                    }
                    //executes if "circle" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "circle") == 0)
                    {
                        String test = m_syntax[1];
                        String[] parameter2 = m_syntax[1].Split(')');
                        if (!test.Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String two = parameter2[0];
                            if (two != null || two != "" || two != " ")
                                circle_draw(positionXaxis, positionYaxis, int.Parse(two));
                            else
                                throw new ArgumentException("Circle Takes Only 1 Parameter");

                        }
                    }

                    else if (string.Compare(m_syntax[0].ToLower(), "square") == 0)
                    {
                        String test = m_syntax[1];
                        String[] parameter2 = m_syntax[1].Split(')');
                        if (!test.Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            String two = parameter2[0];
                            if (two != null || two != "" || two != " ")
                                square_draw(positionXaxis, positionYaxis, int.Parse(two));
                            else
                                throw new ArgumentException("Square Takes Only 1 Parameter");

                        }
                    }
                    //executes if "triangle" command is triggered
                    else if (string.Compare(m_syntax[0].ToLower(), "triangle") == 0)
                    {
                        String[] parameter1 = m_syntax[1].Split(',');
                        //String[] parameter2 = m_syntax[1].Split(',');
                        if (parameter1.Length != 2)
                            throw new Exception("Triangle Takes Only 2 Parameters");
                        else if (!parameter1[parameter1.Length - 1].Contains(')'))
                            throw new Exception(" " + "Missing Paranthesis!!");
                        else
                        {
                            shapefactory sh = new shapefactory();
                            Shape c1 = sh.GetShape("triangle");
                            String[] parameter3 = parameter1[1].Split(')');
                            String one = parameter1[0];
                            // String two = parameter2[0];
                            String three = parameter3[0];
                            if (parameter1.Length > 1 && parameter1.Length < 3)
                                triangle_draw(positionXaxis, positionYaxis, int.Parse(one), int.Parse(three));
                            else
                                throw new ArgumentException("Triangle Takes Only 2 Parameters");
                        }
                    
                }
                return true;
                }
                else
                {
                    return true;
                }
            }
            catch (ArgumentException ecp)
            {
                textBox1.Text = "Error in Line:" + (lineCount) + " " + ecp.Message;
                panel1.Refresh();
                return false;
            }

            catch (Exception ee)
            {
                textBox1.Text = "Error in Line:" + (lineCount) + " " + "parameter Error!!" + ee.Message;
                panel1.Refresh();
                return false;
            }

        }
        /// <summary>
        /// method to clear the command
        /// </summary>
        public void clear()
        {
            clear_bool = false;
            panel1.Refresh();
            textBox2.Clear();
            clear_bool = true;
        }
        /// <summary>
        /// method to reset the button
        /// </summary>
        public void reset()
        {
            reset_bool = false;
            positionXaxis = 0;
            positionYaxis = 0;
            reset_bool = true;
        }
       /// <summary>
       /// method to draw rectangle
       /// </summary>
       /// <param name="a"></param>
       /// <param name="b"></param>
       /// <param name="c"></param>
       /// <param name="d"></param>
        public void rectangle_draw(int a, int b, int c, int d)
        {
            draw = false;
            Rectangle mane = new Rectangle();
            mane.saved_values(a, b, c, d);
            Graphics g = panel1.CreateGraphics();
            mane.Draw_shape(g);
            draw = true;
        }
        /// <summary>
        /// method to draw square
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public void square_draw(int a, int b, int c)
        {
            draw = false;
            Square mane2 = new Square();
            mane2.saved_values(a, b, c);
            //Graphics g = panel1.CreateGraphics();
            //mane2.Draw_shape(g);
            draw = true;

            Square snj2 = new Square();
            snj2.saved_values(a, b, c);
            Graphics g = panel1.CreateGraphics();
            snj2.Draw_shape(g);
        }
         /// <summary>
         /// method to draw circle
         /// </summary>
         /// <param name="a"></param>
         /// <param name="b"></param>
         /// <param name="c"></param>
       
        public void circle_draw(int a, int b, int c)
        {
            draw = false;
            Circle mane2 = new Circle();
            mane2.saved_values(a, b, c);
            Graphics g = panel1.CreateGraphics();
            mane2.Draw_shape(g);
            draw = true;
        }

        /// <summary>
        /// method to drae recangle
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void triangle_draw(int a, int b, int c, int d)
        {
            draw = false;
            Triangle mane4 = new Triangle();
            mane4.saved_values(a, b, c, d);
            Graphics g = panel1.CreateGraphics();
            mane4.Draw_shape(g);
            draw = true;
        }

        /// <summary>
        /// triggered when reset button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button1_Click(object sender, EventArgs e)
        {
            reset();
        }
       
        private void button3_Click(object sender, EventArgs e)
        {
            clear();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        /// <summary>
        /// methos to load the file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       
        public void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            load = false;
            OpenFileDialog loadFileDialog = new OpenFileDialog();
            loadFileDialog.Filter = "Text File (.txt)|*.txt";
            loadFileDialog.Title = "Open File...";

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamReader streamReader = new System.IO.StreamReader(loadFileDialog.FileName);
                textBox2.Text = streamReader.ReadToEnd();
                streamReader.Close();
            }
            load = true;
        }
       /// <summary>
       /// method to save the file
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        public void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save = true;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text File (.txt)| *.txt";
            saveFileDialog.Title = "Save File...";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter fWriter = new StreamWriter(saveFileDialog.FileName);
                fWriter.Write(textBox2.Text);
                fWriter.Close();


            }
            save = true;

        }
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        



        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// method for entering from textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (textBox1.Text == "clear")
            {
                clear();
            }

            if (textBox1.Text == "reset")
            {
                reset();
            }


            if (textBox1.Text == "run")
            {
                variableDict.Clear();
                method = false;

                lineCount = 1;
               // panel1.Refresh();
                textBox1.Clear();
                execute = false;
                var multi_command = textBox2.Text;
                string[] multi_syntax = multi_command.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                String command = textBox2.Text;
                String[] multiLine = command.Split(')');

                for (int i = 0; i < multiLine.Length - 1; i++)
                {
                    String abc = multiLine[i].Trim();
                    m_syntax = abc.Split('(');

                    if (m_syntax[0].Equals("pen") == true)
                    {
                        this.penswitcher(m_syntax[1]);
                    }

                    else if (m_syntax[0].Equals("fill") == true)
                    {
                        if (m_syntax[1] == "on")
                        {
                            fill = true;

                        }

                        if (m_syntax[1] == "off")
                        {
                            fill = false;

                        }

                    }

                }



                foreach (string l in multi_syntax)
                {


                    bool result = caseRun(l);
                    if (!result)
                    {
                        panel1.Invalidate();
                        break;
                    }


                    lineCount++;

                }


            }

        }

        private void aboutToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("This is a program designed to produce different shapes" +
             "\n"
             + "from the user input taken from textbox. For e.g: circle,"
             + "\n" + "triangle, rectangle and so on. Moveto moves the position and" +
             "reset changes position to initial. Command run executes all the program." +
             "Clear clears the drawing area. Drawto draws a line from the size given." + "\n" +
             "Designed by: Sabina Baniya");

        }

        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Instructions: \n" +
                "For Example: \n Programs: circle(70) || triangle(80, 50, 100) || " +
                "rectangle(50,50)  || moveto(50,50 ||drawto(50,50) || fill(red)" +
                "|| fill(off) || pen(blue) || pen(green) || pen(yellow) || pen(red)" +
                "|| r=30 \n count=1\n loop forcount <=5\n circle(l,b)\n r+20\n count+1\nendloop\n " +
                "Similarly, methods also can be made." +
                "Commands: run || clear || reset");

        }
    }
}
    