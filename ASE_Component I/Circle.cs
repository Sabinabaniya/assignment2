﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Component_I
{
 
    class Circle: Shape
    {
        private int r, x, y;
        /// <summary>
        /// method to save values
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public void saved_values(int a, int b, int c) {
            x = a;
            y = b;
            r = c;
        }
        /// <summary>
        /// method to draw shape
        /// </summary>
        /// <param name="g"></param>
        public override void Draw_shape(Graphics g)
        {
            Pen mew2 = Form1.defaultpen;
            SolidBrush me = Form1.sb;
            if(Form1.fill)
            {
                g.FillEllipse(me, x, y, r, r);
            }
            else
            {
                g.DrawEllipse(mew2, x, y, r, r);
            }
           
        }
    }
}
