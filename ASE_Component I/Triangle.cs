﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Component_I
{
    class Triangle : Shape
    {

        private int x, y, bas, per, height;
        /// <summary>
        /// method to save the values
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void saved_values(int a, int b, int c, int d)
        {
            x = a;
            y = b;
            bas = c;
            per = d;
            


        }
        /// <summary>
        /// methos to draw shape
        /// </summary>
        /// <param name="g"></param>
        public override void Draw_shape(Graphics g)
        {
            PointF A = new PointF(x, y);
            PointF B = new PointF(x + per, y);
            PointF C = new PointF(B.X, B.Y + per);
            PointF[] bro = { A, B, C };
            // g.DrawPolygon (mew3,bro);

            Pen pen = Form1.defaultpen;
            SolidBrush brush = Form1.sb;
            if (Form1.fill)
            {
                g.FillPolygon(brush, bro);          //filled triangle is produced.
            }
            else
            {
                g.DrawPolygon(pen, bro);
            }
        }
    }
    }


