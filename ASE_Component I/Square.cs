﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Component_I
{
    class Square:Shape
    {
        private int length, x, y;
       /// <summary>
       /// method to save values
       /// </summary>
       /// <param name="a"></param>
       /// <param name="b"></param>
       /// <param name="c"></param>
        public  void saved_values(int a, int b, int c)
        {
            x = a;
            y = b;
            length = c;
        }


        /// <summary>
        /// method to draw shape
        /// </summary>
        /// <param name="g"></param>
        public override void Draw_shape(Graphics g)
        {
            //Pen mew2 = new Pen(Color.Black,2);
            // g.DrawEllipse(mew2, x, y, r, r);



            Pen mew2 = Form1.defaultpen;
            SolidBrush me = Form1.sb;
            if (Form1.fill)
            {
                g.FillRectangle(me, x, y, length, length);     //gives filled ellipse 
            }
            else
            {
                g.DrawRectangle(mew2, x, y, length, length);     //gives not filled ellipse
            }

        }

       
    }
}
